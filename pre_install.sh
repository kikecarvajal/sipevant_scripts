#!/bin/bash
RED='\033[1;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'

echo -e "${YELLOW}"
echo -e "-----------------------------------------------------------------------"
echo -e "version: 1"
echo -e "${GREEN}"
echo -e "Proyecto: SIPEVANT"
echo -e "${YELLOW}"
echo -e "maintenance: UNINDETEC "
echo -e "UNIDAD DE INVESTIGACION Y DESARROLLO TECNOLOGICO DE LA ARMADA DE MEXICO"
echo -e "------------------------------------------------------------------------"
echo -e "${RED}"
echo -e "---------------"
echo -e "update and upgrade "
echo -e "---------------"
echo -e "${GREEN}"
sudo apt-get update
echo -e "${RED}"
echo -e "----------------"
echo -e "install packages "
echo -e "----------------"
echo -e "${GREEN}"
sudo apt-get install git tree vim wget cmake python3-pip -y
echo -e "${RED}"
echo -e "----------------"
echo -e "packages python"
echo -e "----------------"
echo -e "${GREEN}"
sudo pip3 install rpi.gpio && sudo pip3 install Flask
echo -e "${RED}"
echo -e "--------------------------"
echo -e "create scripts enable gpio"
echo -e "--------------------------"
echo -e "${GREEN}"
touch enableGPI.sh
echo "sudo chmod 777 /dev/gpiomem" > enableGPI.sh
sudo chmod 777 enableGPI.sh